﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _012_yield
{
    public class ListNode<T> : IEnumerable<T>
    {
        public T value;
        public ListNode<T> Next;

        public ListNode<T> Add(T value)
        {
            Next = new ListNode<T> { value = value };
            return Next;
        }

        public IEnumerator<T> GetEnumerator()
        {
            ListNode<T> node = this;
            while(node != null)
            {
                yield return node.value;
                node = node.Next;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public override string ToString()
        {
            return value.ToString();
        }
    }
}