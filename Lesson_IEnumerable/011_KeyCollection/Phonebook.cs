﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _011_KeyCollection
{
    class Phonebook : KeyedCollection<string, Contact>
    {
        protected override string GetKeyForItem(Contact item)
        {
            return item.phone;
        }

        protected override void InsertItem(int index, Contact item)
        {
            base.InsertItem(index, item);
        }
    }
}