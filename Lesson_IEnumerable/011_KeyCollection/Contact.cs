﻿namespace _011_KeyCollection
{
    class Contact
    {
        public string name;
        public string surname;

        public string FullName => $"{surname} {name}";

        public string phone;

        public override string ToString()
        {
            return FullName;
        }
    }
}