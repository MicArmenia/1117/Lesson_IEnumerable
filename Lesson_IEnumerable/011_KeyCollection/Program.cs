﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace _011_KeyCollection
{
    class Program
    {
        static void Main(string[] args)
        {
            var c1 = new Contact { name = "A1", surname = "A1yan", phone = "+37493202020" };
            var c2 = new Contact { name = "A2", surname = "A2yan", phone = "+37455202020" };
            var c3 = new Contact { name = "A3", surname = "A3yan", phone = "+374910202020" };
            var c4 = new Contact { name = "A4", surname = "A4yan", phone = "+374910202020" };

            Phonebook pb = new Phonebook();
            pb.Add(c1);
            pb.Add(c2);
            pb.Add(c3);
            pb.Add(c4);

            Contact c11 = pb[1];
            Contact c12 = pb["+37455202020"];
        }
    }
}
