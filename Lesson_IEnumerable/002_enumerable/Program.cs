﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _002_enumerable
{
    class Program
    {
        static void Main(string[] args)
        {
            ListNode root = new ListNode
            {
                value = 10,
                Next = new ListNode
                {
                    value = 20,
                    Next = new ListNode
                    {
                        value = 30
                    }
                }
            };
        }
    }
}
