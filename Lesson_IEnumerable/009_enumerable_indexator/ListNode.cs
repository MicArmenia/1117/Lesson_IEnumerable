﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace _009_enumerable_indexator
{
    public class ListNode<T> : IEnumerable<T>
    {
        public T value;
        public ListNode<T> Next;

        public ListNode<T> Add(T value)
        {
            Next = new ListNode<T> { value = value };
            return Next;
        }

        public T this[int index]
        {
            get { return GetValue(index); }
        }

        private T GetValue(int index)
        {
            int i = 0;
            foreach (T item in this)
            {
                if (i == index)
                    return item;
                i++;
            }

            return default(T);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new Enumerator(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public override string ToString()
        {
            return value.ToString();
        }

        private class Enumerator : IEnumerator<T>
        {
            public Enumerator(ListNode<T> root)
            {
                _root = root;
            }

            private ListNode<T> _root;

            public T Current { get; private set; }

            object IEnumerator.Current => Current;

            public bool MoveNext()
            {
                if (_root == null)
                    return false;

                Current = _root.value;
                _root = _root.Next;

                return true;
            }

            public void Reset()
            {
                _root = null;
            }

            public void Dispose()
            {
                Reset();
            }
        }
    }
}