﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _009_enumerable_indexator
{
    class Program
    {
        static void Main(string[] args)
        {
            var root = new ListNode<int> { value = 10 };
            root
                .Add(10)
                .Add(20)
                .Add(1000)
                .Add(40);

            int value = root[3];
        }
    }
}
