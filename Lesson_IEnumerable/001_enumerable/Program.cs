﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _001_enumerable
{
    class Program
    {
        static void Main(string[] args)
        {
            ListNode root = new ListNode { value = 10 };
            ListNode node1 = new ListNode { value = 20 };

            root.Next = node1;

            ListNode node2 = new ListNode { value = 30 };
            node1.Next = node2;
        }
    }
}