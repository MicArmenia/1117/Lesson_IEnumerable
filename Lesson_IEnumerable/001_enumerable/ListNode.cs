﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _001_enumerable
{
    class ListNode
    {
        public int value;
        public ListNode Next;

        public override string ToString()
        {
            return value.ToString();
        }
    }
}