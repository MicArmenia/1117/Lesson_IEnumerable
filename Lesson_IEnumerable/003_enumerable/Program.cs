﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _003_enumerable
{
    class Program
    {
        static void Main(string[] args)
        {
            ListNode root = new ListNode { value = 10 };
            root.Add(20);
            root.Next.Add(30);
            root.Next.Next.Add(40);
        }
    }
}
