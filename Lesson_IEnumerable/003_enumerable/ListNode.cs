﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _003_enumerable
{
    class ListNode
    {
        public int value;
        public ListNode Next;

        public void Add(int value)
        {
            Next = new ListNode { value = value };
        }

        public override string ToString()
        {
            return value.ToString();
        }
    }
}