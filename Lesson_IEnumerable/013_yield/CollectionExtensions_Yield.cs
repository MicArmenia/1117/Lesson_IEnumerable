﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _013_yield
{
    static class CollectionExtensions_Yield
    {
        public static IEnumerable<int> EvenNumbers(this IEnumerable<int> source)
        {
            foreach (int item in source)
                if (item % 2 == 0)
                    yield return item;
        }

        public static IEnumerable<int> LargerNumbers(this IEnumerable<int> source, int number)
        {
            foreach (int item in source)
                if (item > number)
                    yield return item;
        }

        public static int Sum(this IEnumerable<int> source)
        {
            int sum = 0;
            foreach (int item in source)
                sum += item;
            return sum;
        }
    }
}
