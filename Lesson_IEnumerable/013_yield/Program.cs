﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _013_yield
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = { 15, 25, 22, 30, 80, 55, 70};

            var items1 = arr.EvenNumbers();
            var items2 = arr.LargerNumbers(35);
            //var items3 = arr.GetEvenAndLargerNumbers(35);

            var items3 = arr
                .EvenNumbers()
                .LargerNumbers(18);

            foreach (int item in items3)
            {
                Console.WriteLine(item);
            }

            int sum = arr
                .EvenNumbers()
                .LargerNumbers(45)
                .Sum();

            Console.ReadLine();
        }
    }
}
