﻿using System.Collections.Generic;

namespace _013_yield_old
{
    static class CollectionExtensions
    {
        public static List<int> GetEvenNumbers(this IEnumerable<int> source)
        {
            List<int> items = new List<int>();
            foreach (int item in source)
            {
                if (item % 2 == 0)
                    items.Add(item);
            }

            return items;
        }

        public static List<int> GetLargerNumbers(this IEnumerable<int> source, int number)
        {
            List<int> items = new List<int>();
            foreach (int item in source)
            {
                if (item > number)
                    items.Add(item);
            }

            return items;
        }

        public static List<int> GetEvenAndLargerNumbers(this IEnumerable<int> source, int number)
        {
            List<int> items = new List<int>();
            foreach (int item in source)
            {
                if (item % 2 == 0 && item > number)
                    items.Add(item);
            }

            return items;
        }
    }
}
