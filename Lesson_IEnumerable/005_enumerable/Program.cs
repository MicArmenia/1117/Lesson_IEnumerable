﻿namespace _005_enumerable
{
    class Program
    {
        static void Main(string[] args)
        {
            ListNode root = new ListNode { value = 10 };
            root
                .Add(10)
                .Add(20)
                .Add(1000)
                .Add(40);

            int max = root.value;
            ListNode node = root.Next;
            while (node != null)
            {
                if (node.value>max)
                {
                    max = node.value;
                }
                node = node.Next;
            }
        }
    }
}