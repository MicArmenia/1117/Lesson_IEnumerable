﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _005_enumerable
{
    class ListNode
    {
        public int value;
        public ListNode Next;

        public ListNode Add(int value)
        {
            Next = new ListNode { value = value };
            return Next;
        }

        public override string ToString()
        {
            return value.ToString();
        }
    }
}