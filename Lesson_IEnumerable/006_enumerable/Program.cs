﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _006_enumerable
{
    class Program
    {
        static void Main(string[] args)
        {
            ListNode root = new ListNode { value = 10 };
            root
                .Add(10)
                .Add(20)
                .Add(1000)
                .Add(40);

            foreach (int value in root)
            {
                //
            }

            IEnumerator enumerator = root.GetEnumerator();
            while(enumerator.MoveNext())
            {
                int value = (int)enumerator.Current;
                //
            }
            enumerator.Reset();
        }
    }
}
