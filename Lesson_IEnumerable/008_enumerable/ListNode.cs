﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _008_enumerable
{
    public class ListNode<T> : IEnumerable<T>
    {
        public T value;
        public ListNode<T> Next;

        public ListNode<T> Add(T value)
        {
            Next = new ListNode<T> { value = value };
            return Next;
        }

        public IEnumerator<T> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public override string ToString()
        {
            return value.ToString();
        }

        private class Enumerator : IEnumerator<T>
        {
            public Enumerator(ListNode<T> root)
            {
                _root = root;
            }

            private ListNode<T> _root;

            public T Current { get; private set; }

            object IEnumerator.Current => Current;

            public bool MoveNext()
            {
                if (_root == null)
                    return false;

                Current = _root.value;
                _root = _root.Next;

                return true;
            }

            public void Reset()
            {
                _root = null;
            }

            public void Dispose()
            {
                Reset();
            }
        }
    }
}