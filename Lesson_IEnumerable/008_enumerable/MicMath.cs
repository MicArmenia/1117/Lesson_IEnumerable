﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _008_enumerable
{
    public static class MicMath
    {
        public static int Max(IEnumerable<int> arr)
        {
            int max = 0;
            foreach (int item in arr)
            {
                if (item > max)
                    max = item;
            }

            return max;
        }
    }
}