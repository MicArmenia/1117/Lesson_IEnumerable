﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _008_enumerable
{
    class Program
    {
        static void Main(string[] args)
        {
            var root = new ListNode<int> { value = 10 };
            root
                .Add(10)
                .Add(20)
                .Add(1000)
                .Add(40);

            var root1 = new ListNode<int>
            {
                10,
                20,
                50
            };

            //long[] arr = { 10, 20, 1000, 40 };
            int[] arr = { 10, 20, 1000, 40 };
            var list = new List<int> { 10, 20, 1000, 40 };

            int max1 = MicMath.Max(root);
            int max2 = MicMath.Max(arr);
            int max3 = MicMath.Max(list);
        }
    }
}
