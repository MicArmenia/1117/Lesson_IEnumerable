﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _007_enumerable
{
    public class ListNode : IEnumerable
    {
        public int value;
        public ListNode Next;

        public ListNode Add(int value)
        {
            Next = new ListNode { value = value };
            return Next;
        }

        public IEnumerator GetEnumerator()
        {
            return new Enumerator(this);
        }

        public override string ToString()
        {
            return value.ToString();
        }

        private class Enumerator : IEnumerator
        {
            public Enumerator(ListNode root)
            {
                _root = root;
            }

            private ListNode _root;

            public object Current { get; private set; }

            public bool MoveNext()
            {
                if (_root == null)
                    return false;

                Current = _root.value;
                _root = _root.Next;

                return true;
            }

            public void Reset()
            {
                _root = null;
            }
        }
    }
}