﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _007_enumerable
{
    public static class MicMath
    {
        public static int Max(IEnumerable arr)
        {
            int max = 0;
            foreach (int item in arr)
            {
                if (item > max)
                    max = item;
            }

            return max;
        }

        //public static int Max(int[] arr)
        //{
        //    int max = 0;
        //    foreach (int item in arr)
        //    {
        //        if (item > max)
        //            max = item;
        //    }

        //    return max;
        //}

        //public static int Max(List<int> arr)
        //{
        //    int max = 0;
        //    foreach (int item in arr)
        //    {
        //        if (item > max)
        //            max = item;
        //    }

        //    return max;
        //}

        //public static int Max(ListNode arr)
        //{
        //    int max = 0;
        //    foreach (int item in arr)
        //    {
        //        if (item > max)
        //            max = item;
        //    }

        //    return max;
        //}
    }
}